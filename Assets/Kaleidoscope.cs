﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace UnityStandardAssets.ImageEffects
{
    [System.Serializable]
    public class ControlValue
    {
        public string name;
        public float min;
        public float max;
        public float speed;
        public float acceleration;
        public float home;
        public float homeSpeed;
        public float value;
    }

    public class Kaleidoscope : MonoBehaviour
    {

        float FovMin = 2;
        float FovMax = 7;
        float FovMultiplier = 1;
        float tileMin = 0.1f;
        float tileMax = 0.3f;

        public float scale = 1;
        public float autoSwitchTime = 10 * 60;
        float lastSwitchTime;
        public bool auto = true;

        public Renderer mat;

        public Material[] materials;
        int matnum = 0;

        float t;
        Vector2 offset;
        Vector2 tile;
        public UnityEngine.UI.Text mmss;
        string timeFormat = "{0:000}:{1:00}";
        float m = 2; // multiplier


        [SerializeField]
        public ControlValue[] controls;
        Dictionary<string, ControlValue> controlValues;

        Fisheye fisheye;
        Tonemapping tonemapping;
        ColorCorrectionCurves colorCurves;
        VignetteAndChromaticAberration chroma;
        Bloom bloom;

        float timeDirection = 1;

        // Use this for initialization
        void Start()
        {
            controlValues = new Dictionary<string, ControlValue>();
            foreach (var v in controls)
            {
                controlValues[v.name] = v;
            }

            fisheye = FindObjectOfType<Fisheye>();
            tonemapping = FindObjectOfType<Tonemapping>();
            colorCurves = FindObjectOfType<ColorCorrectionCurves>();
            chroma = FindObjectOfType<VignetteAndChromaticAberration>();
            bloom = FindObjectOfType<Bloom>();
            t = 0;
        }


        float ModifyValue(ControlValue v, float input)
        {
            if (input == 0)
            {
                if (v.value > v.home)
                {
                    input = (((v.home - v.value) / (v.max - v.home))) - 0.01f;

                    //input = (v.home - v.value) * ((v.max-v.home)) * 0.01f;
                } else
                {
                     input = (((v.home - v.value) / (v.home - v.min))) + 0.01f;

                    //input = (v.home - v.value) * ((v.home-v.min)) * 0.01f;
                }

                v.value = Mathf.Clamp(v.value + (input * v.homeSpeed * Time.deltaTime) + (Mathf.Abs(v.value - v.home) * v.acceleration * v.homeSpeed * input * Time.deltaTime), v.min, v.max);

            }
            else
            {
                v.value = Mathf.Clamp(v.value + (input * v.speed * Time.deltaTime) + (Mathf.Abs(v.value - v.home) * v.acceleration * input * Time.deltaTime), v.min, v.max);

            }




            return v.value;
        }

        Color minCol, maxCol;
        float controlRot;
        int verbose = 1;

        // Update is called once per frame
        void Update()
        {
 
            if (Input.GetButton("Start") || Time.time < 5)
            {
                float speed = 10;
                if (Time.time < 5)
                {
                    speed = 5;
                }
                foreach(var v in controlValues)
                {
                    v.Value.value = Mathf.MoveTowards(v.Value.value,v.Value.home,Time.deltaTime*v.Value.speed* speed);
                }
            }

            fisheye.strengthY = ModifyValue(controlValues["fisheye"], Input.GetAxis("X2"));
            fisheye.strengthX = (fisheye.strengthY - controlValues["fisheye"].home) * 0.3f;

            //tonemapping.exposureAdjustment = ModifyValue(controlValues["tonemapping"], Mathf.Abs(Input.GetAxis("Bumpers")));
            chroma.chromaticAberration = ModifyValue(controlValues["chroma"], Input.GetAxis("Y2"));
            scale = ModifyValue(controlValues["scale"], Input.GetAxis("Y1"));
            bloom.bloomThreshold = ModifyValue(controlValues["bloom"], -Mathf.Abs(Input.GetAxis("X")+ Input.GetAxis("Y")+ Input.GetAxis("A")+ Input.GetAxis("B")));
            controlRot += ModifyValue(controlValues["rot"], Input.GetAxis("Triggers"));
            colorCurves.saturation = ModifyValue(controlValues["saturation"], Input.GetAxis("Y3"));
            chroma.blur = ModifyValue(controlValues["blur"], Input.GetAxis("X3"));
            chroma.blurDistance = chroma.blur;
            chroma.blurSpread = chroma.blur * 0.5f;
            chroma.intensity = chroma.blur;

            FovMultiplier = ModifyValue(controlValues["fov"], Input.GetAxis("Bumpers")) * 0.01f;

            float rA = Input.GetAxis("B");
            float gA = Input.GetAxis("A");
            float bA = Input.GetAxis("X");

            float lowMin = -5;
            float lowMax = 0.2f;
            float highMin = -5;
            float highMax = 2;
            float colSpeed = 10;
            float colRetnSpeed = 3;
            if (rA != 0 || gA != 0 || bA !=0)
            {
                // one or more of rgb is being pressed. the others go to -10,0.5

                // whichever ones are pressed go to -1,2

                if (rA == 0)
                {
                    minCol.r += (lowMin - minCol.r) * Time.deltaTime * colSpeed;
                    maxCol.r += (lowMax - maxCol.r) * Time.deltaTime * colSpeed;
                }
                else
                {
                    minCol.r += (highMin - minCol.r) * Time.deltaTime * colSpeed;
                    maxCol.r += (highMax - maxCol.r) * Time.deltaTime * colSpeed;
                }

                if (gA == 0)
                {
                    minCol.g += (lowMin - minCol.g) * Time.deltaTime * colSpeed;
                    maxCol.g += (lowMax - maxCol.g) * Time.deltaTime * colSpeed;
                }
                else
                {
                    minCol.g += (highMin - minCol.g) * Time.deltaTime * colSpeed;
                    maxCol.g += (highMax - maxCol.g) * Time.deltaTime * colSpeed;
                }
                if (bA == 0)
                {
                    minCol.b += (lowMin - minCol.b) * Time.deltaTime * colSpeed;
                    maxCol.b += (lowMax - maxCol.b) * Time.deltaTime * colSpeed;
                }
                else
                {
                    minCol.b += (highMin - minCol.b) * Time.deltaTime * colSpeed;
                    maxCol.b += (highMax*4 - maxCol.b) * Time.deltaTime * colSpeed;
                }

            }
            else
            {
                minCol.r += (0 - minCol.r) * Time.deltaTime * colRetnSpeed;
                maxCol.r += (1 - maxCol.r) * Time.deltaTime * colRetnSpeed;
                minCol.g += (0 - minCol.g) * Time.deltaTime * colRetnSpeed;
                maxCol.g += (1 - maxCol.g) * Time.deltaTime * colRetnSpeed;
                minCol.b += (0 - minCol.b) * Time.deltaTime * colRetnSpeed;
                maxCol.b += (1 - maxCol.b) * Time.deltaTime * colRetnSpeed;
            }

            AnimationCurve r = new AnimationCurve(new Keyframe[] { new Keyframe(0, minCol.r), new Keyframe(1, maxCol.r) });
            AnimationCurve g = new AnimationCurve(new Keyframe[] { new Keyframe(0, minCol.g), new Keyframe(1, maxCol.g) });
            AnimationCurve b = new AnimationCurve(new Keyframe[] { new Keyframe(0, minCol.b), new Keyframe(1, maxCol.b) });
            
            // 1, 0, 2



            colorCurves.redChannel = r;
            colorCurves.greenChannel = g;
            colorCurves.blueChannel = b;
            colorCurves.UpdateParameters();

            bool autoSwitch = false;
            if (Time.time > lastSwitchTime + autoSwitchTime)
            {
                lastSwitchTime = Time.time;
                autoSwitch = true;
            }

            if (Input.GetKeyDown("1") || Input.GetButtonDown("Y") || autoSwitch)
            {
                matnum++; matnum %= materials.Length; mat.sharedMaterial = materials[matnum];
            }
            if (Input.GetKeyDown("2"))
            {
                matnum--; matnum += materials.Length; matnum %= materials.Length; mat.sharedMaterial = materials[matnum];
            }

            t += Time.deltaTime * 1f * timeDirection;

            t += Input.GetAxis("X1") * Time.deltaTime * 5 * (Input.GetKey(KeyCode.Space) ? 25 : 1);

            if (Input.GetAxis("X1") > 0) timeDirection = 1;
            if (Input.GetAxis("X1") < 0) timeDirection = -1;

            //float dt = Time.deltaTime;
            mat.sharedMaterial.SetTextureOffset("_MainTex", offset);
            mat.sharedMaterial.SetTextureScale("_MainTex", tile * scale);


            string outputString = "";

            if (verbose > 0)
            {
                outputString += string.Format(timeFormat, (((int)t) / 60), (((int)t) % 60)) + " " + materials[matnum].name;
            }


            if (verbose > 1)
            {
                outputString +=
       "\n X1 " + Input.GetAxis("X1").ToString("0.00")
     + " Y1 " + Input.GetAxis("Y1").ToString("0.00")
     + "\n X2 " + Input.GetAxis("X2").ToString("0.00")
     + " Y2 " + Input.GetAxis("Y2").ToString("0.00")
     + "\n X3 " + Input.GetAxis("X3").ToString("0.00")
     + " Y3 " + Input.GetAxis("Y3").ToString("0.00")
     + "\n X4 " + Input.GetAxis("X4").ToString("0.00")
     + " Y4 " + Input.GetAxis("Y4").ToString("0.00")
     + "\n Triggers  " + Input.GetAxis("Triggers").ToString("0.00")
     + "\n Bumpers  " + Input.GetAxis("Bumpers").ToString("0.00")
     + "\n X  " + Input.GetAxis("X")
     + "\n Y  " + Input.GetAxis("Y")
     + "\n A  " + Input.GetAxis("A")
     + "\n B  " + Input.GetAxis("B")
     + "\n MinCol  " + minCol
     + "\n MaxCol  " + maxCol
    ;
                foreach(var cv in controlValues)
                {
                    if (cv.Value.name != "")
                    {
                        int cur = (int)(Mathf.InverseLerp(cv.Value.min, cv.Value.max, cv.Value.value) * 100);
                        int def = (int)(Mathf.InverseLerp(cv.Value.min, cv.Value.max, cv.Value.home) * 100);
                        outputString += "\n " + cv.Value.name + " cur:" + cur + "% def:" + def+"%";
                    }
                }

            }


            if (Input.GetKeyDown("v"))
            {
                verbose++; verbose %= 3;
            }

            mmss.text = outputString;


            if (auto)
            {
                offset.x = Mathf.Sin(m * t * 0.0031f) * 3f;
                offset.y = Mathf.Sin(0.5f + m * t * 0.0021f) * 3f;

                tile.x = tileMin + (Mathf.Sin(m * t * 0.02f) * 0.4f + 0.9f) * (Mathf.Sin(m * t * 0.03f) * 0.4f + 0.9f) * (tileMax - tileMin);
                tile.y = tile.x;


                Camera.main.transform.rotation = Quaternion.Euler(0, 0, Mathf.Sin(m * t * 0.001f) * 1500 + controlRot);
                Camera.main.orthographicSize = FovMultiplier * (FovMin + (Mathf.Sin(-m * t * 0.037f) * 0.5f + 0.5f) * (FovMax - FovMin));



            }
            else
            {
                offset.x += Input.GetAxis("Mouse X") * 0.01f;
                offset.y += Input.GetAxis("Mouse Y") * 0.01f;
                Camera.main.transform.rotation = Quaternion.Euler(0, 0, (offset.x + offset.y) * 100);

            }

        }
    }

}